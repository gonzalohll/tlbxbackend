
exports.unicaFuncion = async (req, res) => {
  /*
      Recibe como parámetro: {text:''}
      Devuelve el mismo objeto que recibe como parámetro pero con el valor de text invertido.
      Es necesario que el parametro tenga el formato indicado.
  */
  let params = req.query
  const esPalindromo = texto => {
    const textoMinusculas = texto.toLowerCase()
    const textoReverso = textoMinusculas.split('').reverse().join('')
    return textoReverso === textoMinusculas
  }
  if (params.text && params.text.length) {
    if (esPalindromo(params.text)) {
      params = {
        ...params,
        palindrome: true
      }
    }
    params.text = params.text.split('').reverse().join('')
    return res.status(200).send(params)
  } else {
    return res.status(400).send({ error: 'no text' })
  }
}