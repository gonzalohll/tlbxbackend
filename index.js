const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())
app.use(express.json({ extended: true }))

const port = process.env.port || 4000;

// importar rutas
app.use('/iecho', require('./routes/iecho'))

app.listen(port, '0.0.0.0', () => {
  console.log('El servidor esta funcionando en el puerto 4000')
})

module.exports = app
