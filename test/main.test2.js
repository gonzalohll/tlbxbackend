const request = require("supertest")("http://localhost:4000");
const expect = require("chai").expect;

describe("GET /iecho", function () {
  it('Devuelve estado 200, siempre que se envíe como parametro "text":""', async () => {
    const response = await request.get("/iecho?text=test");
    expect(response.status).to.eql(200);
  });
  it('Devuelve estado 200 y un objeto {"text":""} , siempre que se envíe como parametro "text":""', async () => {
    const response = await request.get("/iecho?text=test");
    expect(response.status).to.eql(200);
    expect(response.body).to.have.property('text')
  });
  it('Devuelve estado 200 y un objeto {"text":""} cuyo valor será el reverso del valor recibido como parametro', async () => {
    const response = await request.get("/iecho?text=test");
    expect(response.status).to.eql(200);
    expect(response.body).to.have.property('text')
    expect(response.body.text).to.eql('tset')
  });
  it('Devuelve un objeto {"error":"no text"} con estado 400, siempre que se envíen otros parámetros', async () => {
    const response = await request.get("/iecho?txt=test");
    expect(response.status).to.eql(400);
    expect(response.body).to.have.property('error')
    expect(response.body.error).to.eql('no text')
  });
});