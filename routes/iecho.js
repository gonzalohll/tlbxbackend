// Rutas autenticar usuarios
const express = require('express')
const router = express.Router()
const mainController = require('../controllers/mainController')

// Ruta al método del controlador
router.get('/',
  mainController.unicaFuncion
)

module.exports = router
